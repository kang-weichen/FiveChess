package Service;

import java.util.ArrayList;

import Dao.UserDao;
import pojo.User;

public class UserService {
	private UserDao userDao = UserDao.getInstance();
	
	//单例
	private static UserService userService = new UserService();
	private UserService() {}
	public static UserService getInstance() {
		return userService;
	}
	
	public boolean add(User user) {
		return userDao.add(user);
	}
	
	public User search(User user) {
		return userDao.search(user);
	}
	
	public ArrayList<User> list(){
		return userDao.list();
	}
	
	public User login(User user) {
		User u = userDao.search(user);

		if(u == null) {
			return null;
		}else if(u.getPassword().equals(user.getPassword())) {
			return u;
		}else {
			return null;
		}
	}
	
	public boolean cover(User user) {
		return this.userDao.cover(user);
	}
}
