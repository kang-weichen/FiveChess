package Table;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import Service.GameService;
import pojo.Game;
import pojo.User;

public class GamesTable extends AbstractTableModel{

	private String[] colNames = {"","甲方","乙方","胜者","对局时间"};
	private Object[][] data;
	//service
	private GameService gameService = GameService.getInstance();
	
	
	//构造方法
	public GamesTable() {
		ArrayList<Game> games = gameService.list();
		
		if(games != null&& games.size() != 0) {
			this.data = new Object[games.size()][colNames.length];
			for(int i=0;i<games.size();i++) {
				data[i][0] = new Boolean(false);
				data[i][1] = games.get(i).getJia();
				data[i][2] = games.get(i).getYi();
				data[i][3] = (games.get(i).getJia_win())? games.get(i).getJia():games.get(i).getYi();
				data[i][4] = games.get(i).getDate();
			}
		}else {
			this.data = new Object[0][0];
		}
	}
	
	//返回结果行数
	@Override
	public int getRowCount() {
		return this.data.length;
	}
	//返回结果列数
	@Override
	public int getColumnCount() {
		return this.colNames.length;
	}
	//获得对应列名
	@Override
	public String getColumnName(int column) {
		return this.colNames[column];
	}
	//每列对应的类型
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return data[0][columnIndex].getClass();
	}
	//返回哪行哪列对应的数据
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return data[rowIndex][columnIndex];
	}
	/**
	 * 哪列不能编辑
	 * 返回false 不可编辑
	 */
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if (columnIndex == 0) {		//编号不能编辑
			return true;
		} else {
			return false;
		}
	}
	//修改表格中的数据
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		data[rowIndex][columnIndex] = aValue;
		/*修改表格数据后，显示改后结果*/
		fireTableCellUpdated(rowIndex, columnIndex);
	}
	@Override
	public void fireTableRowsDeleted(int firstRow, int lastRow) {
		super.fireTableRowsDeleted(firstRow, lastRow);
	}
}
