package UI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Service.UserService;
import pojo.User;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JPasswordField;

public class ChangePasswordUI extends JFrame {
	private UserService userService = UserService.getInstance();
	
	private JPanel contentPane;
	private JTextField textField_2;
	private JPasswordField passwordField;
	private JPasswordField passwordField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChangePasswordUI frame = new ChangePasswordUI(new User());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ChangePasswordUI(User user) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 489, 371);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("您的密码为初始密码，请更改");
		lblNewLabel.setFont(new Font("黑体", Font.PLAIN, 20));
		lblNewLabel.setBounds(107, 35, 273, 44);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("密码");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_1.setFont(new Font("宋体", Font.PLAIN, 20));
		lblNewLabel_1.setBounds(107, 106, 71, 30);
		contentPane.add(lblNewLabel_1);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(188, 106, 166, 28);
		contentPane.add(passwordField);
		
		JLabel lblNewLabel_1_1 = new JLabel("确认密码");
		lblNewLabel_1_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_1_1.setFont(new Font("宋体", Font.PLAIN, 20));
		lblNewLabel_1_1.setBounds(92, 161, 86, 30);
		contentPane.add(lblNewLabel_1_1);
		
		passwordField_1 = new JPasswordField();
		passwordField_1.setBounds(188, 161, 166, 28);
		contentPane.add(passwordField_1);
		
		JLabel lblNewLabel_1_2 = new JLabel("手机号");
		lblNewLabel_1_2.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_1_2.setFont(new Font("宋体", Font.PLAIN, 20));
		lblNewLabel_1_2.setBounds(107, 216, 71, 30);
		contentPane.add(lblNewLabel_1_2);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(188, 216, 166, 28);
		contentPane.add(textField_2);
		
		JButton btnNewButton = new JButton("确认修改");
		btnNewButton.setFont(new Font("宋体", Font.PLAIN, 20));
		btnNewButton.setBounds(107, 270, 120, 38);
		contentPane.add(btnNewButton);
		btnNewButton.addActionListener((e)->{
			String s1 = passwordField.getText();
			String s2 = passwordField_1.getText();
			String phone = textField_2.getText();
			//全输入了
			if(s1.length()>0&&s2.length()>0&&phone.length()>0) {
				//密码一样
				if(s1.equals(s2)) {
					User u = this.userService.search(user);
					u.setPassword(s1);
					
					if(this.userService.cover(u)) {
						new LoginUI().setVisible(true);
						setVisible(false);
					}
				}else {
					JOptionPane.showMessageDialog(null, "密码不一致");
				}
			}else {
				JOptionPane.showMessageDialog(null,"请正确输入");
			}
		});
		
		JButton btnNewButton_1 = new JButton("返回");
		btnNewButton_1.setFont(new Font("宋体", Font.PLAIN, 20));
		btnNewButton_1.setBounds(251, 270, 113, 38);
		contentPane.add(btnNewButton_1);
		btnNewButton_1.addActionListener((e)->{
			new LoginUI().setVisible(true);
			setVisible(false);
		});
		
		//窗口居中
		setLocationRelativeTo(null);
	}
}
