package UI;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import Service.GameService;
import Table.GamesTable;
import pojo.Chess;
import pojo.Game;
import pojo.User;

public class HistoryUI extends JFrame{
	private Game game = null;
	
	private GameService gameService = GameService.getInstance();
	
	private JTable table;
	private JPanel contentPane;
	
	private GamesTable gamesTable;
	 
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HistoryUI frame = new HistoryUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public HistoryUI() {
		this.gamesTable = new GamesTable();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		//表格
		table = new JTable(this.gamesTable);
		table.setBounds(557, 102, 398, 335);
		contentPane.add(table);
		//加滚动条
		JScrollPane jp = new JScrollPane(table);
		jp.setBounds(659, 102, 296, 335);
		contentPane.add(jp);
		
		JButton btnNewButton = new JButton("复盘");
		btnNewButton.setFont(new Font("宋体", Font.PLAIN, 20));
		btnNewButton.setBounds(719, 499, 74, 34);
		contentPane.add(btnNewButton);
		btnNewButton.addActionListener((e)->{
			int cnt=0;
			for(int i=0;i<this.gamesTable.getRowCount();i++) {
				if(this.gamesTable.getValueAt(i, 0).equals(true)) {
					cnt++;
				}
			}
			if(cnt!=1) JOptionPane.showMessageDialog(null, "请单选");
			else {
				//取出这盘棋，展示
				for(cnt=0;cnt<this.gamesTable.getRowCount();cnt++) {
					if(this.gamesTable.getValueAt(cnt, 0).equals(true)) {
						break;
					}
				}
				//得到cnt
				Game game = this.gameService.list().get(cnt);
				this.game = game;
				//得到game了，展示！
				repaint();
				
			}
		});
		
		JButton btnNewButton_3 = new JButton("返回");
		btnNewButton_3.setFont(new Font("宋体", Font.PLAIN, 20));
		btnNewButton_3.setBounds(829, 499, 74, 34);
		contentPane.add(btnNewButton_3);
		btnNewButton_3.addActionListener((e)->{
			new LoginUI().setVisible(true);
			this.setVisible(false);
		});
		
		//窗口居中	
		setLocationRelativeTo(null);
	}
	
	public void paint(Graphics g) {
		super.paint(g);
		
		//加背景图片
		BufferedImage image = null;
		try {
			image = ImageIO.read(new File("p.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		g.drawImage(image, 70, 70, contentPane);
		
		//绘制棋盘线
		g.drawRect(120, 120, 480, 480);
		for(int i=1;i<=14;i++) {
			g.drawLine(120, 120+32*i, 600, 120+32*i);
			g.drawLine(120+32*i, 120, 120+32*i, 600);
		}
		
		//根据集合绘制棋子
		ArrayList<Chess> chesses = (this.game ==null)? new ArrayList<>() : this.game.getChesses();
		
		for(int i=0;i<chesses.size();i++) {
			if(i==0) {//第一个棋子
				int xx = chesses.get(i).getX()*32+120-15;
				int yy = chesses.get(i).getY()*32+120-15;
				if(chesses.get(i).getBlack()) {
					g.setColor(Color.BLACK);
				}else {
					g.setColor(Color.WHITE);
				}
				g.fillOval(xx, yy, 30, 30);
				
				//棋子序号
				if(i%2 == 0) {//black
					g.setColor(Color.WHITE);
					g.drawString(i+1+"", xx+12, yy+17);
				}else {
					g.setColor(Color.BLACK);
					g.drawString(i+1+"", xx, yy);
				}
				
			}else {
				try {
					Thread.sleep( chesses.get(i).getDate().getTime()-chesses.get(i-1).getDate().getTime());
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				int xx = chesses.get(i).getX()*32+120-15;
				int yy = chesses.get(i).getY()*32+120-15;
				if(chesses.get(i).getBlack()) {
					g.setColor(Color.BLACK);
				}else {
					g.setColor(Color.WHITE);
				}
				g.fillOval(xx, yy, 30, 30);
				
				if(i%2 == 0) {//black
					g.setColor(Color.WHITE);
					g.drawString(i+1+"", xx+12, yy+17);
				}else {
					g.setColor(Color.BLACK);
					g.drawString(i+1+"", xx+12, yy+17);
				}
			}
		}
	}
}
