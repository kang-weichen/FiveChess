package UI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Service.UserService;
import pojo.User;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;

public class LoginUI extends JFrame{
	private UserService userService = UserService.getInstance();
	
	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginUI frame = new LoginUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginUI() {
		this.setTitle("登录系统");
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 540, 415);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("五子棋登录界面");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("黑体", Font.PLAIN, 30));
		lblNewLabel.setForeground(Color.DARK_GRAY);
		lblNewLabel.setBounds(77, 31, 381, 46);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("账号");
		lblNewLabel_1.setFont(new Font("宋体", Font.PLAIN, 20));
		lblNewLabel_1.setBounds(128, 111, 54, 36);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("密码");
		lblNewLabel_1_1.setFont(new Font("宋体", Font.PLAIN, 20));
		lblNewLabel_1_1.setBounds(128, 177, 54, 36);
		contentPane.add(lblNewLabel_1_1);
		
		textField = new JTextField();
		textField.setBounds(229, 111, 166, 31);
		contentPane.add(textField);
		textField.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(229, 177, 166, 31);
		contentPane.add(passwordField);
		
		JButton btnNewButton = new JButton("登录");
		btnNewButton.setFont(new Font("宋体", Font.PLAIN, 20));
		btnNewButton.setBounds(147, 273, 97, 36);
		contentPane.add(btnNewButton);
		btnNewButton.addActionListener((e)->{
			String number = textField.getText();
			String password = passwordField.getText();
			if(number.length() == 0||password.length() == 0) {
				 JOptionPane.showMessageDialog(null, "请输入账号和密码");
			}else {
				//输入了东西
				User user = new User(number, password, null, null);
				
				if(userService.login(user) == null) {
					JOptionPane.showMessageDialog(null, "账号或密码错误!");
				}else {
					//登录成功
					//改密码
					if(user.getPassword().equals("0")) {
						new ChangePasswordUI(user).setVisible(true);
						setVisible(false);
					}
					
					if(userService.login(user).getIsgod()) {
						//记录界面
						new RecordUI().setVisible(true);
						setVisible(false);
					}else {
						//监督 注册、回放界面
						new HistoryUI().setVisible(true);
						setVisible(false);
					}
				}	
			}
		});
		
		JButton btnNewButton_1 = new JButton("退出");
		btnNewButton_1.setFont(new Font("宋体", Font.PLAIN, 20));
		btnNewButton_1.setBounds(280, 273, 97, 36);
		contentPane.add(btnNewButton_1);
		btnNewButton_1.addActionListener((e)->{
			System.exit(0);
		});
		
		//窗口居中
		setLocationRelativeTo(null);
	}
}
