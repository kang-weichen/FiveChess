package UI;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Service.GameService;
import Service.UserService;
import pojo.Chess;
import pojo.Game;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

public class RecordUI extends JFrame implements MouseListener{
	private UserService userService = UserService.getInstance();
	private GameService gameService = GameService.getInstance();
	
	private Game game = null;
	
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JRadioButton rdbtnNewRadioButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RecordUI frame = new RecordUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RecordUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton_2 = new JButton("保存当前信息");
		btnNewButton_2.setHorizontalAlignment(SwingConstants.LEFT);
		btnNewButton_2.setFont(new Font("宋体", Font.PLAIN, 20));
		btnNewButton_2.setBounds(752, 246, 170, 40);
		contentPane.add(btnNewButton_2);
		btnNewButton_2.addActionListener((e)->{
			if(this.game!=null){
					JOptionPane.showMessageDialog(null, "存档成功");
			}
			else JOptionPane.showMessageDialog(null, "未进行游戏");
		});
		
		JButton btnNewButton_3 = new JButton("退出");
		btnNewButton_3.setFont(new Font("宋体", Font.PLAIN, 20));
		btnNewButton_3.setBounds(748, 561, 121, 34);
		contentPane.add(btnNewButton_3);
		btnNewButton_3.addActionListener((e)->{
			new LoginUI().setVisible(true);
			setVisible(false);
		});
		
		JLabel lblNewLabel = new JLabel("比赛选手用户名：");
		lblNewLabel.setBounds(662, 10, 116, 27);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("甲方");
		lblNewLabel_1.setBounds(664, 53, 58, 15);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("乙方");
		lblNewLabel_1_1.setBounds(667, 89, 58, 15);
		contentPane.add(lblNewLabel_1_1);
		
		textField = new JTextField();
		textField.setBounds(740, 51, 96, 21);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(740, 86, 96, 21);
		contentPane.add(textField_1);
		
		JLabel lblNewLabel_2 = new JLabel("比赛时间");
		lblNewLabel_2.setBounds(661, 139, 58, 15);
		contentPane.add(lblNewLabel_2);
		
		textField_2 = new JTextField();
		textField_2.setBounds(741, 137, 136, 21);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblNewLabel_1_2 = new JLabel("甲方");
		lblNewLabel_1_2.setBounds(658, 185, 58, 15);
		contentPane.add(lblNewLabel_1_2);
		
		this.rdbtnNewRadioButton = new JRadioButton("黑棋");
		rdbtnNewRadioButton.setBounds(724, 181, 67, 23);
		contentPane.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_2 = new JRadioButton("白棋");
		rdbtnNewRadioButton_2.setBounds(806, 176, 67, 23);
		contentPane.add(rdbtnNewRadioButton_2);
		
		ButtonGroup g1 = new ButtonGroup();
		g1.add(rdbtnNewRadioButton);
		g1.add(rdbtnNewRadioButton_2);
		rdbtnNewRadioButton.setSelected(true);
		
		JLabel lblNewLabel_3 = new JLabel("现在执子方");
		lblNewLabel_3.setBounds(654, 261, 88, 15);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("棋子落位信息：");
		lblNewLabel_4.setBounds(662, 342, 96, 15);
		contentPane.add(lblNewLabel_4);
		
		JButton btnNewButton = new JButton("开始");
		btnNewButton.setBounds(772, 501, 97, 23);
		contentPane.add(btnNewButton);
		btnNewButton.addActionListener((e)->{
			
			String s1 = textField.getText();
			String s2 = textField_1.getText();
			String date = textField_2.getText();
			
			if(s1.length()>0&&s2.length()>0&&date.length()>0) {
				this.game = new Game(s1, s2, null, true, true, date, new ArrayList<Chess>());
				gameService.add(game);
				this.repaint();
			}
		});
		
		
		//鼠标监听
		this.addMouseListener(this);
		//窗口居中	
		setLocationRelativeTo(null);
	}
	
	public void paint(Graphics g) {
		super.paint(g);
		
		//加背景图片
		BufferedImage image = null;
		try {
			image = ImageIO.read(new File("p.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		g.drawImage(image, 70, 70, contentPane);
		
		//绘制棋盘线
		g.drawRect(120, 120, 480, 480);
		for(int i=1;i<=14;i++) {
			g.drawLine(120, 120+32*i, 600, 120+32*i);
			g.drawLine(120+32*i, 120, 120+32*i, 600);
		}

		//绘制棋子
		ArrayList<Chess> chesses = (this.game ==null)? new ArrayList<>() : this.game.getChesses();
		for(int i=0;i<chesses.size();i++) {
			int xx = chesses.get(i).getX()*32+120-15;
			int yy = chesses.get(i).getY()*32+120-15;
			
			if(chesses.get(i).getBlack()) {
				g.setColor(Color.BLACK);
			}else {
				g.setColor(Color.WHITE);
			}
			g.fillOval(xx, yy, 30, 30);
			
			//棋子序号
			if(i%2 == 0) {//black
				g.setColor(Color.WHITE);
				g.drawString(i+1+"", xx+12, yy+17);
			}else {
				g.setColor(Color.BLACK);
				g.drawString(i+1+"", xx, yy);
			}
		}
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
//		System.out.println("鼠标点击"+"("+e.getX()+","+e.getY()+")");
	}
	@Override
	public void mousePressed(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		
		if(this.game!=null&&this.game.getJia_win() == null&&(x>110)&&(x<610)&&(y>110)&&(y<610)) {
			//找点
			int keyx=0,keyy=0,dis=32;
			for(int i=0;i<16;i++) {
				if(abs(120+32*i,x)<dis) {
					dis = abs(120+32*i,x);
					keyx = i;
				}
			}
			dis=32;
			for(int i=0;i<16;i++) {
				if(abs(120+32*i,y)<dis) {
					dis =abs(120+32*i,y);
					keyy = i;
				}
			}
			
			System.out.println("鼠标点击 ("+keyx+","+keyy+")");
			//判断是否下了
			if(gameService.turn(game)[keyx][keyy]!=0) {
				JOptionPane.showMessageDialog(null, "此点已有棋子");
			}else {
				//写到文件里
				Chess chess = new Chess(this.game.isBlackNext(), keyx, keyy, new Date());
				this.game = this.gameService.update(game, chess);
			}
			//repaint
			this.repaint();
			
			//判断是否游戏结束,winer赋值
			int result = this.gameService.judge(this.gameService.turn(this.game), keyx, keyy);
			System.out.println("对战结果:"+result);
			if(result == 1) {
				JOptionPane.showMessageDialog(null, "黑方胜利");
				this.game.setJia_win(false);
				this.gameService.cover(this.game);
				this.game = null;
			}else if(result == -1) {
				JOptionPane.showMessageDialog(null, "白方胜利");
				this.game.setJia_win(true);
				this.gameService.cover(this.game);
				this.game = null;
			}
		}
	}
	@Override
	public void mouseReleased(MouseEvent e) {}
	@Override
	public void mouseEntered(MouseEvent e) {}
	@Override
	public void mouseExited(MouseEvent e) {}
	
	public static int abs(int a,int b) {return a>b? a-b:b-a;}
}
