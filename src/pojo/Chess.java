package pojo;

import java.util.Date;

public class Chess {
	Boolean Black;
	int x;
	int y;
	Date date;
	
	public Chess() {
		super();
	}
	public Chess(Boolean black, int x, int y, Date date) {
		super();
		Black = black;
		this.x = x;
		this.y = y;
		this.date = date;
	}
	public Boolean getBlack() {
		return Black;
	}
	public void setBlack(Boolean black) {
		Black = black;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
}
