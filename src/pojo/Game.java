package pojo;

import java.util.ArrayList;
import java.util.Date;

public class Game {
	String jia;
	String yi;
	Boolean jia_win;
	boolean jia_black;
	boolean blackNext;
	String date;
	ArrayList<Chess> chesses;
	
	public Game() {
		super();
	}
	public Game(String jia, String yi, Boolean jia_win, boolean jia_black, boolean blackNext, String date,
			ArrayList<Chess> chesses) {
		super();
		this.jia = jia;
		this.yi = yi;
		this.jia_win = jia_win;
		this.jia_black = jia_black;
		this.blackNext = blackNext;
		this.date = date;
		this.chesses = chesses;
	}
	@Override
	public String toString() {
		return "{\"jia\":\"" + jia + "\",\"yi\":\"" + yi + "\",\"jia_win\":\"" + jia_win + "\",\"jia_black\":\""
				+ jia_black + "\",\"blackNext\":\"" + blackNext + "\",\"date\":\"" + date + "\",\"chesses\":\""
				+ chesses + "\"}";
	}
	public String getJia() {
		return jia;
	}
	public void setJia(String jia) {
		this.jia = jia;
	}
	public String getYi() {
		return yi;
	}
	public void setYi(String yi) {
		this.yi = yi;
	}
	public Boolean getJia_win() {
		return jia_win;
	}
	public void setJia_win(Boolean jia_win) {
		this.jia_win = jia_win;
	}
	public boolean isJia_black() {
		return jia_black;
	}
	public void setJia_black(boolean jia_black) {
		this.jia_black = jia_black;
	}
	public boolean isBlackNext() {
		return blackNext;
	}
	public void setBlackNext(boolean blackNext) {
		this.blackNext = blackNext;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public ArrayList<Chess> getChesses() {
		return chesses;
	}
	public void setChesses(ArrayList<Chess> chesses) {
		this.chesses = chesses;
	}
	
}
