package pojo;

public class User {
	String id;
	String password;
	Boolean isgod;//记录（god）负责下棋，监督负责回放和用户管理
	String phone;
	
	public User() {
		super();
	}
	public User(String id, String password, Boolean isgod, String phone) {
		super();
		this.id = id;
		this.password = password;
		this.isgod = isgod;
		this.phone = phone;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Boolean getIsgod() {
		return isgod;
	}
	public void setIsgod(Boolean isgod) {
		this.isgod = isgod;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	@Override
	public String toString() {
		return "{\"id\":\"" + id + "\",\"password\":\"" + password + "\",\"isgod\":\"" + isgod + "\",\"phone\":\""
				+ phone + "\"}";
	}
}
